<?php
class ProfilePicture{
    public $id;
    public $name;
    public $image;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","profilepicexam7");
    }

    public function prepare($data=array()){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }if(array_key_exists("image",$data)){
            $this->image=$data["image"];
        }
    }
    public function store(){
        $query="INSERT INTO `profilepicexam7`.`userdata` (`name`, `image`) VALUES ('".$this->name."', '".$this->image."')";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function index(){
        $alldp=array();

        $query="Select * from `profilepicexam7`.`userdata`";
        if($result=mysqli_query($this->conn,$query)){
            while($row=mysqli_fetch_assoc($result)){
                $alldp[]=$row;
            }
            return $alldp;
        }
    }
    public function view(){
        $query="SELECT * from `profilepicexam7`.`userdata` where `id`=".$this->id;
        $row=mysqli_fetch_assoc(mysqli_query($this->conn,$query));
        return $row;
    }
    public function delete(){
        $query="DELETE FROM `profilepicexam7`.`userdata` WHERE `userdata`.`id` =".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function update(){
        $query="UPDATE `profilepicexam7`.`userdata` SET `name` = '".$this->name."', `image` = '".$this->image."' WHERE `userdata`.`id`=".$this->id;
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }

    public function activate(){
        if($this->deactivate()){
            $time=time();
            $query="UPDATE `profilepicexam7`.`userdata` SET `active` = '".$time."' WHERE `userdata`.`id` =".$this->id;
            if(mysqli_query($this->conn,$query)){
                return true;
            }else return false;
        }
    }
    public function deactivate(){
        $query="UPDATE `profilepicexam7`.`userdata` SET `active` = NULL WHERE 1";
        if(mysqli_query($this->conn,$query)){
            return true;
        }else return false;
    }
    public function findactive(){
    $query="Select * from `profilepicexam7`.`userdata` WHERE `active` IS NOT NULL";
        if($result=mysqli_query($this->conn,$query)){
         $row=mysqli_fetch_assoc($result);
            return $row;
        }
}
}