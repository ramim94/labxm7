<?php
include_once('../ProfilePicture.php');
$dp=new ProfilePicture();
$dp->prepare($_POST);
$singleData=$dp->view();

if(isset($_FILES) && !empty($_FILES['image']['name'])){
    $filename=time().$_FILES['image']['name'];
    $temploc=$_FILES['image']['tmp_name'];
    move_uploaded_file($temploc , $_SERVER['DOCUMENT_ROOT']."/Ramim_136876_batch22_labexam7/images/".$filename);
    $_POST['image']=$filename;
    $dp->prepare($_POST);
    unlink($_SERVER['DOCUMENT_ROOT']."/Ramim_136876_batch22_labexam7/images/".$singleData['image']);
    if($dp->update()){
    $_SESSION['message']="data successfully updated";
    header('location:index.php');
}else{
    $_SESSION['message']="failed to update data";
    header('location:index.php');
}
}else{
    $_POST['image']=$singleData['image'];
    $dp->prepare($_POST);
    if($dp->update()){
        $_SESSION['message']="data successfully updated";
        header('location:index.php');
    }else{
        $_SESSION['message']="failed to update data";
        header('location:index.php');
    }
}