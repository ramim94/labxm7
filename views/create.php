<!DOCTYPE html>
<html lang="en">
<head>
    <title>Create</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>New Profile Picture: </h2>
    <form role="form" method="post" enctype="multipart/form-data" action="store.php">
        <div class="form-group">
            <label >Name:</label>
            <input type="text" class="form-control" name="name" placeholder="Enter name">
        </div>

        <div class="form-group">
            <label >Image:</label>
            <input type="file" class="form-control" name="image" >
        </div>

        <button type="submit" class="btn btn-default" >Submit</button>
    </form>
</div>

</body>
</html>

