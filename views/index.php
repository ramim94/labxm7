<?php
session_start();
include_once('../ProfilePicture.php');

$dp=new ProfilePicture();
$alldp=$dp->index();
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Index</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<?php echo $_SESSION['message'];
$_SESSION['message']="";?>
<br>

<div class="container">
    <div class="list-group">
        <?php foreach($alldp as $chk){
            if(!is_null($chk['active'])){
                $activeid=$chk['id'];
  ?>
        <ul class="list-group">
            <li class="list-group-item list-group-item-info">Profile Picture</li>
            <li class="list-group-item"> <img src="../images/<?php echo $chk['image'] ?>" > </li>
        </ul>

<?php      }}?>

    </div>
</div>

<div class="container">
    <h2>All Picture List</h2>

    <a href="create.php" class="btn btn-info" role="button">Create</a>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($alldp as $dp){
            $sl=1;?>
        <tr>
            <td> <?php echo $sl ?> </td>
            <td><?php echo $dp['id'] ?> </td>
            <td> <?php echo $dp['name'] ?> </td>
            <td> <img src="../images/<?php echo $dp['image'] ?>" > </td>
            <td>
                <a href="activate.php?id=<?php echo $dp['id']?>" class="<?php if($activeid==$dp['id']){echo "btn btn-danger";}
                else{echo "btn btn-info";} ?>"
                     role="button" >Change State</a>
                <a href="view.php?id=<?php echo $dp['id']?>" class="btn btn-info" role="button">View</a>
                <a href="edit.php?id=<?php echo $dp['id']?>" class="btn btn-primary" role="button">Edit</a>
                <a href="delete.php?id=<?php echo $dp['id']?>"  class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
        <?php $sl++; }?>
        </tbody>
    </table>
</div>

</body>
</html>

