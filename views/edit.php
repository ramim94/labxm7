<?php
include_once('../ProfilePicture.php');
$dp=new ProfilePicture();
$dp->prepare($_GET);
$singleData=$dp->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Profile Picture: </h2>

    <form role="form" method="post" enctype="multipart/form-data" action="update.php">

        <input type="hidden" name="id" value=" <?php echo $singleData['id']?> ">
        <div class="form-group">
            <label >Name:</label>
            <input type="text" class="form-control" name="name" value="<?php echo $singleData['name']?> ">
        </div>

        <div class="form-group">
            <label >Image:</label>
            <input type="file" class="form-control" name="image" >
        </div>
        <img <img src="../images/<?php echo $singleData['image'] ?>" >

        <button type="submit" class="btn btn-default" >Submit</button>
    </form>
</div>

</body>
</html>


